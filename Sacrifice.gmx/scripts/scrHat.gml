////hat
var hat = argument0
with hat{
    x=other.x
    y=other.y-sprite_get_height(other.sprite_index)/2-sprite_height/4
    image_angle=other.image_angle
    image_xscale=other.image_xscale
}
