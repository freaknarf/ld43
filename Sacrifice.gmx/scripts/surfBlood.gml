surfBld=argument0
obj=argument1
surface_set_target(surfBld)
if instance_exists(obj)
with obj{

image_angle=direction
if (speed<1.5 )

draw_sprite_ext(sprite_index,image_index,x,y,image_xscale,image_yscale,image_angle,c_red,1)

}
surface_reset_target()
if surface_exists(surfBld)
draw_surface(surfBld,0,0)

