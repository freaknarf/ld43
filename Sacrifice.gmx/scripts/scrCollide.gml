// Vertical
repeat(abs(yspd)) {
    if (!place_meeting(x, y + sign(yspd), oWall))
        y += sign(yspd); 
    else {
        yspd = 0;
        break;
    }
}

// Horizontal
repeat(abs(xspd)) {
    if (!place_meeting(x + sign(xspd), y, oWall))
        x += sign(xspd); 
    else {
        xspd = 0;
        break;
    }
}
