if invincible=false{
    life--
    invincible=true
    alarm[0]=room_speed/2
    scrKnockback (x-1-other.x,y-1-other.y)
    scrScreenshake(0,0)
    instance_create(0,0,oSleep)
    instance_create(0,0,oFlash)
    repeat (25)   scrBloodJuice(oBadBlood)
    audio_play_sound(HURT,1,0)
}
