// script for creating the path that the enemy instances are going to follow, so make a new script resource

var sx =x
var sy =y
target=oPlayer
if instance_exists(target){
var fx =target.x
var fy =target.y
}else{//motherfuckinfuck
var fx =x
var fy =y
}

if !mp_grid_path(global.AI_grid,global.path[id],sx,sy,fx,fy,true)
{
//show_message("no path !!!")
return false;
}

else
{
path_set_kind(global.path[id],0)
//path_set_precision(global.path[id],8)
return true
}

/*This code gets the start and finish coordinates for our path and then uses them in conjunction with the function mp_grid_path(). This function will calculate a path between the two given points and returns true if one is found (ie: no obstacles "flagged" in the grid block it) or it will return false if none is found. Note that if the function returns true, it also creates the path for you and assigns it to the path resource that you use as argument1 (in this case the global path we defined at the start).

So, our code first tries to create a path through the mp_grid, and if none is found, it tells us with a message (you would normally have some "failsafe" code in there to catch this and deal with it, but for our tutorial, a simple message is fine). However if one is found, it sets the path type (1 to make a "smooth" path), and sets the precision to 8 to make the path as smooth as possible. this step isn't really necessary, but I find that it gives the path a better "feel".

Our script returns true or false depending on the outcome of the path creation check, which we will use as an additional check later in our tutorial game*/
