if keyboard_check_pressed(vk_down)
menuItem++
if keyboard_check_pressed(vk_up)
menuItem--
validate= keyboard_check_pressed(vk_space) or keyboard_check_pressed(vk_enter) or mouse_check_button_pressed(mb_left)
menuItem=clamp(menuItem,0,ds_list_size(menuList)-1)

if validate 
switch menuItem
{
case 0 : 
room_goto_next()
break;
case 1 : 
room_goto(rHelp)
break;
case 2 : 
audio_stop_all()
break;
case 3 : 
game_end()
break;
}
