scrCooldown()
var inst=0
if canShoot=true
if (attack)
or (autofire)
{
   idleTime=0
   switch (weaponType){
      case 0://"knife":
      shootDelay=10
      if !instance_exists(oKnife)
            inst=instance_create(x,y,oKnife);
      break;
      case 1://"throwable knives":
shootDelay=15      
var xx=x + lengthdir_x(16, point_direction(x, y,cursor.x,cursor.y));
var yy=y + lengthdir_y(16, point_direction(x, y,cursor.x,cursor.y))
            inst=instance_create(x,y,oThrowableKnives);
      break;      
      case 2://"spire":
shootDelay=20
var xx=x + lengthdir_x(32, point_direction(x, y,cursor.x,cursor.y));
var yy=y + lengthdir_y(32, point_direction(x, y,cursor.x,cursor.y))

           inst=instance_create(xx,yy,oSpire);
           break;
      case 3://"sword":
      shootDelay=25
      var xx=x + lengthdir_x(16, point_direction(x, y,cursor.x,cursor.y));
      var yy=y + lengthdir_y(16, point_direction(x, y,cursor.x,cursor.y))
            
           if instance_exists(oSword)
              instance_destroy(oSword)
           inst=instance_create(xx,yy,oSword);
           break;
      break;
      case 4://"ninja star":
      shootDelay=15
      var xx=x + lengthdir_x(16, point_direction(x, y,cursor.x,cursor.y));
var yy=y + lengthdir_y(16, point_direction(x, y,cursor.x,cursor.y))
          inst=instance_create(xx,yy,oNinjaStar)
      break;
      case 6://"arrow":
      shootDelay=30
      break;
      case 7://"shotgun":
      shootDelay=20
      break;
      case 8://"cannon":
      shootDelay=100
      break;        
      case 9://"cannon":
      shootDelay=50
      break;        
          
      
      }
   if instance_exists(inst){
   inst.type=weaponType
   inst.owner=id
   scrCollide()
   scrKnockback (sign(x-cursor.x),sign(y-cursor.y))
   }
   canShoot=false
}
