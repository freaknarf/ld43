//draw_sprite_outlined

if argument0=c_white
shader_set(shader_shot)
draw_sprite_ext(sprite_index,image_index,x+1,y,image_xscale,image_yscale,image_angle,argument0,1)
draw_sprite_ext(sprite_index,image_index,x-1,y,image_xscale,image_yscale,image_angle,argument0,1)
draw_sprite_ext(sprite_index,image_index,x,y+1,image_xscale,image_yscale,image_angle,argument0,1)
draw_sprite_ext(sprite_index,image_index,x,y-1,image_xscale,image_yscale,image_angle,argument0,1)
if argument0=c_white
shader_reset()
draw_self()

